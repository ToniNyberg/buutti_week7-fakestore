import React from 'react'
import {BrowserRouter, Route, Routes} from "react-router-dom"
import CategoryProducts from './components/Category'
import Homepage from './components/Homepage'

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
      <Route path="/" element={<Homepage />}>
      </Route>
        <Route path="/products/category/:category" element={<CategoryProducts />}>
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
