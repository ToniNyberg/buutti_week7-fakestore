import React, {useState, useEffect} from 'react'
import {useParams} from "react-router-dom"
import "../index.css"


const CategoryProducts = () => {
    const [products, setProducts] = useState([]);
    const {category} = useParams()

    useEffect(()=> {
        const fetchCategory = async () => {
            console.log(category)
            const res = await fetch(`https://fakestoreapi.com/products/category/${category}`);
            const jsonData = await res.json()
            setProducts(jsonData);
        }
       fetchCategory()
    },[category])

    

  return (
    <>
    <div className="grid-container">
        {products.map((prod) => {
const {id, title, description, image, category} = prod
return(
    <div key={id} className="box">
    <div className="details">
    <h5>{title}</h5>
    <h4>{category}</h4>
<   p>{description}</p>
    </div>
    <img src={image} alt="" />
    </div>
)
})}
    </div>
    </>
  )
}

export default CategoryProducts