import React, {useState, useEffect} from 'react';
import "../index.css"

const Homepage = () => {
    const [fake, setFake] = useState([]);

    
    const fakestore = async () => {
        const res = await fetch("https://fakestoreapi.com/products");
        const jsonData = await res.json()
        setFake(jsonData);
    }
    useEffect(()=> {
        fakestore();
    },[])
    
  return (
    <>
        <h1>Fake store</h1>
        <div className="grid-container">
            {fake.map((values)=> {
                const {id, title, description, image, category} = values
                return(
                    <div key={id} className="box">
                    <div>
                    <h5>{title}</h5>
                    <h4>{category}</h4>
                <   p>{description}</p>
                    </div>
                    <img src={image} alt="" />
                    </div>
                )
            })}
            
        </div>
    </>
  )
}

export default Homepage